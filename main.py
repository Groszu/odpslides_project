"""
__author__ = 'Radosław Grosz'
__copyright___ = "Copyright (c) 2020 Radosław Grosz"
__email__ = 'radekgrosz@o2.pl'

"""


from odpslides.presentation import Presentation
from PyQt5.QtWidgets import QLineEdit, QPushButton, QVBoxLayout, QHBoxLayout, QPlainTextEdit, QButtonGroup, QGroupBox, \
                            QLabel, QGridLayout, QErrorMessage, QMessageBox, QApplication, QWidget, QDialog
import sys
from functools import wraps


def update_text_boxes(f):
    @wraps(f)
    def wrapped(self):
        """
        getting text from text box to variables
        """
        self.text = self.text_on_slide.toPlainText()
        self.title_text = self.title_on_slide.toPlainText()
        self.subtitle_text = self.subtitle_on_slide.toPlainText()
        self.file_name = self.name_of_presentation.text()
        self.image_name = self.name_of_image.text()

        f(self)

        """
        cleaning text boxes
        """
        self.text_on_slide.clear()
        self.title_on_slide.clear()
        self.subtitle_on_slide.clear()
        self.name_of_image.clear()
    return wrapped


class App(QDialog):

    def __init__(self):
        super().__init__()

        self.title = 'Editor'
        self.left = 100
        self.top = 100
        self.width = 600
        self.height = 480
        self.p = Presentation(background_color='darkseagreen', footer="footer")
        self.group_box_menu = QGroupBox("Menu")
        self.group_box_text = QGroupBox("Presentation")
        self.title_on_slide = QPlainTextEdit(self)
        self.subtitle_on_slide = QPlainTextEdit(self)
        self.text_on_slide = QPlainTextEdit(self)
        self.name_of_image = QLineEdit(self)
        self.name_of_presentation = QLineEdit(self)
        self.msg = QMessageBox()
        self.msg.setIcon(QMessageBox.Critical)
        self.file_name = "presentation"
        self.image_name = "no_image.png"
        self.title_text = "title"
        self.subtitle_text = 'subtitle'
        self.text = 'text'
        self.label_presentation_name = QLabel("presentation name")
        self.label_title = QLabel("title")
        self.label_subtitle = QLabel("subtitle")
        self.label_text = QLabel("text")
        self.label_image = QLabel("image name")
        self.text_layout = QVBoxLayout()
        self.box = QHBoxLayout()
        self.menu_layout = QVBoxLayout()
        self.button_add_slide = QPushButton("Add slide", self)
        self.button_add_slide.clicked.connect(self.create_title_slide)
        self.button_add_title_slide = QPushButton("title slide", self)
        self.button_add_title_slide.clicked.connect(self.title_layout)
        self.button_add_text_slide = QPushButton("text slide", self)
        self.button_add_text_slide.clicked.connect(self.text_slide_layout)
        self.button_add_image_slide = QPushButton("image slide", self)
        self.button_add_image_slide.clicked.connect(self.image_slide_layout)
        self.button_add_text_image_slide = QPushButton("text + image slide", self)
        self.button_add_text_image_slide.clicked.connect(self.text_image_slide_layout)
        self.button_save = QPushButton("save", self)
        self.button_save.clicked.connect(self.save)
        self.button_exit = QPushButton("exit", self)
        self.button_exit.clicked.connect(sys.exit)
        self.init_ui()

    def init_ui(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.create_menu_layout()
        self.create_text_layout()
        self.box.addWidget(self.group_box_text)
        self.box.addWidget(self.group_box_menu)
        self.setLayout(self.box)
        self.show()

    def create_menu_layout(self):
        """
        Adding buttons to layout
        """
        buttons = [self.button_add_slide, self.button_add_title_slide, self.button_add_text_slide,
                   self.button_add_text_image_slide, self.button_add_image_slide, self.button_save, self.button_exit]

        for button in buttons:
            self.menu_layout.addWidget(button)

        self.group_box_menu.setLayout(self.menu_layout)

    def create_text_layout(self):
        """
        Adding text boxes to layout
        """

        widgets = [self.label_presentation_name, self.name_of_presentation,
                   self.label_title, self.title_on_slide,
                   self.label_subtitle, self.subtitle_on_slide,
                   self.label_text, self.text_on_slide,
                   self.label_image, self.name_of_image]

        for text in widgets:
            self.text_layout.addWidget(text)

        self.label_image.setHidden(1)
        self.name_of_image.setHidden(1)
        self.label_text.setHidden(1)
        self.text_on_slide.setHidden(1)
        self.group_box_text.setLayout(self.text_layout)

    def check_file_name(self):
        """
        Removing wrong characters from file name
        """

        self.file_name = ''.join(self.file_name.split())
        self.file_name = self.file_name.strip("#%&*:<>?")
        if len(self.file_name) > 255:
            self.msg.setText("Error")
            self.msg.setInformativeText('Presentation name is to long!')
            self.msg.setWindowTitle("Error")
            self.msg.exec_()
            return False
        return True

    def check_image_name(self):
        """
        Checking if image name contains bad characters
        """

        wrong_symbols = ['#', '%', '&', '*', ':', '<', '>', '?']
        for symbol in wrong_symbols:
            if symbol in self.image_name:
                self.msg.setText("Error")
                self.msg.setInformativeText('Bad image name!')
                self.msg.setWindowTitle("Error")
                self.msg.exec_()
                return False
        return True

    def title_layout(self):
        self.button_add_slide.clicked.disconnect()
        self.button_add_slide.clicked.connect(self.create_title_slide)
        self.label_title.setHidden(0)
        self.title_on_slide.setHidden(0)
        self.label_subtitle.setHidden(0)
        self.subtitle_on_slide.setHidden(0)
        self.label_text.setHidden(1)
        self.text_on_slide.setHidden(1)
        self.label_image.setHidden(1)
        self.name_of_image.setHidden(1)

    @update_text_boxes
    def create_title_slide(self):
        self.p.add_title_chart(title=self.title_text, subtitle=self.subtitle_text)

    def text_slide_layout(self):
        self.button_add_slide.clicked.disconnect()
        self.button_add_slide.clicked.connect(self.create_text_slide)
        self.label_title.setHidden(0)
        self.title_on_slide.setHidden(0)
        self.label_subtitle.setHidden(1)
        self.subtitle_on_slide.setHidden(1)
        self.label_text.setHidden(0)
        self.text_on_slide.setHidden(0)
        self.label_image.setHidden(1)
        self.name_of_image.setHidden(1)

    @update_text_boxes
    def create_text_slide(self):
        self.p.add_titled_outline_chart(title=self.title_text, outline=self.text)

    def image_slide_layout(self):
        self.button_add_slide.clicked.disconnect()
        self.button_add_slide.clicked.connect(self.create_image_slide)
        self.label_title.setHidden(0)
        self.title_on_slide.setHidden(0)
        self.label_subtitle.setHidden(1)
        self.subtitle_on_slide.setHidden(1)
        self.label_text.setHidden(1)
        self.text_on_slide.setHidden(1)
        self.label_image.setHidden(0)
        self.name_of_image.setHidden(0)

    @update_text_boxes
    def create_image_slide(self):
        if self.check_image_name():
            self.p.add_titled_image(title=self.title_text, image_file=self.image_name, pcent_stretch_center=80,
                                    pcent_stretch_content=80)

    def text_image_slide_layout(self):
        self.button_add_slide.clicked.disconnect()
        self.button_add_slide.clicked.connect(self.create_text_image_slide)
        self.label_title.setHidden(0)
        self.title_on_slide.setHidden(0)
        self.label_subtitle.setHidden(1)
        self.subtitle_on_slide.setHidden(1)
        self.label_text.setHidden(0)
        self.text_on_slide.setHidden(0)
        self.label_image.setHidden(0)
        self.name_of_image.setHidden(0)

    @update_text_boxes
    def create_text_image_slide(self):
        if self.check_image_name():
            self.p.add_titled_text_and_image(title=self.title_text, outline=self.text,
                                             image_file=self.image_name)

    @update_text_boxes
    def save(self):
        if self.check_file_name():
            self.p.save(filename=self.file_name, launch=0)


if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())

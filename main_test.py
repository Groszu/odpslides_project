from main import *
import unittest
from unittest import TestCase
from unittest.mock import patch


class TestApp(TestCase):

    def setUp(self):
        self.app = QApplication(sys.argv)
        self.test = App()

    def test_check_image_name(self):
        self.test.image_name = "test"
        self.assertTrue(self.test.check_image_name())

    def test_check_image_name2(self):
        self.test.image_name = "test%"
        self.assertFalse(self.test.check_image_name())

    def test_check_file_name(self):
        self.test.file_name = 'name'
        self.assertTrue(self.test.check_file_name())

    def test_check_file_name2(self):
        self.test.file_name = 'abc%'
        self.assertTrue(self.test.check_file_name())


if __name__ == '__main__':
    unittest.main()
